﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1L2_TIO_Solutions
{
    public abstract class Octopus
    {
        private int age;
        private string location;

        public int Age { get => age; set => age = value; }
        public string Location { get => location; set => location = value; }

        public Tag tag { get; set; }

        // OR

        //public int Age { get; set; }
        //public int Location { get; set; }

        public void TimeLapse(int years)
        {
            Age += years;
        }

        public Octopus()
        {

        }

        public Octopus(int age, string location)
        {
            Age = age;
            Location = location;
        }

        public abstract void Hunt();

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1L2_TIO_Solutions
{
    public class GiantOctopus:Octopus,ISwimmer
    {
        public string[] Diet { get; set; }
        public double Weight { get; set; }

        public void Attack()
        {
            Console.WriteLine("Giant Octopus Attack!!");
        }

        public override void Hunt()
        {
            Console.WriteLine("Easy");
        }

        public void Swim()
        {
            Console.WriteLine("Giant Octopus just swam");
        }
    }
}

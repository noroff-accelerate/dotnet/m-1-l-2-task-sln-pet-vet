﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1L2_TIO_Solutions
{
    enum Habitats
    {
        Swamp,
        Island,
        Mountain,
        Plains,
        Forest
    }
}

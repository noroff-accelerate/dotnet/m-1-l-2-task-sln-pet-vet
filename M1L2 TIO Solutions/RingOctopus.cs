﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M1L2_TIO_Solutions
{
    public class RingOctopus:Octopus
    {
        public string VenomType { get; set; }
        public List<string>Antidotes { get; set; }

        public void Bite()
        {
            Console.WriteLine("Should probably check that antidote list");
        }

        public override void Hunt()
        {
            Console.WriteLine("This may take awhile");
        }
    }
}

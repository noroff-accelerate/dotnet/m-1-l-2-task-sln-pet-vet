﻿using System;

namespace M1L2_TIO_Solutions
{
    class Program
    {
        static void Main(string[] args)
        {
            // Try it out! Create a class to represent an animal of your choice. Give it two fields and attributes and one behaviour.
            // See Octopus class in solution explorer


            // Try it out! Create two objects of the class you defined before and give them each values for their properties and their behaviour

            //Octopus octopusObject = new Octopus();
            //octopusObject.Age = 2;
            //octopusObject.Location = "South Pacific";
            //octopusObject.TimeLapse(1);

            //Octopus octopusObject2 = new Octopus();
            //octopusObject.Age = 5;
            //octopusObject.Location = "Mediteranian";
            //octopusObject.TimeLapse(4);

            // Try it out! Add an overloaded constructor and expose the default constructor in your animal class.
            // See Octopus class in solution explorer

            // Try it out! Update the fields in your animal class to use access modifiers.
            // See Octopus class in solution explorer

            // Try it out! Create two child classes for your animal class. Give them each two attributes and at least one behaviour.
            // See GiantOctopus and RingOctopus classes in solution explorer

            // Try it out! Make your animal class abstract
            // See Octopus class in solution explorer

            // Try it out!Add an abstract method to your animal class called Hunt.You can then decide on the method signature and how it is implemented by the child classes.
            // See Octopus class in solution explorer

            // Try it out!Create a new class called Tag.Use aggregation to allow an Animal to have a Tag object for use by game rangers.
            // See Tag and Octopus class in solution explorer

            // Try it out!! Create an Interface of your choice and make one of your Animal child classes implement the interface.
            // See ISwimmer interface in solution explorer

            // Call GoSwim
            GiantOctopus giantOctopus = new GiantOctopus();

            GoSwim(giantOctopus);

        }

        // Try it out! Define a method (public static...) which uses one of your Animal child classes as a parameter.
        // The method should display all of the information about that object i.e. its attribute values.

        public static void ShowDetails(GiantOctopus octopus)
        {
            Console.WriteLine($"Location: {octopus.Location} Weight: {octopus.Weight} Age: {octopus.Age} Diet example: {octopus.Diet[0]}");
        }

        //Try it out! Define a method (public static...) which has a parameter of the interface you created previously.
        //Then call it and pass in an instance of the child class which implements the interface as an argument. See Main()
        public static void GoSwim(ISwimmer swimmer)
        {
            swimmer.Swim();
        }
    }
}
